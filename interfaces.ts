export type loginDataType = {
    username: string;
    password: string;
  };

export type addFoodObject = {
  name: string,
  unit:string,
  amount:number
}

  export type FoodEntryDetails = Record<string, { unit: string, amount: number }>;
  export type FoodEntryCreateOptions = {
    name: string;
    details: FoodEntryDetails;
  };
  export type FoodEntry = FoodEntryCreateOptions & { id: string, createdAt: Date };
  export type FoodEntryUpdateOptions = Partial<FoodEntryCreateOptions>;