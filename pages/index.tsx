import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Container, Typography, Button, Grid } from "@mui/material";
import Header from "../components/header";
import styles from "../styles/Index.module.css";
import { loadState } from "../components/localStorageStates";
import Axios from "axios";
import "react-toastify/dist/ReactToastify.css";
import FoodCard from "../components/foodCard";
import AddFoodModal from "../components/addFoodModal";
import {  FoodEntry } from "../interfaces";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Index = () => {
  const [foods, setFoods] = useState([]);
  const router = useRouter();
  const [open, setOpen] = useState<boolean>(false);
  let token = loadState("token");

  useEffect(() => {
    if (!token) {
      router.push("/login");
    }
  }, [token, router]);

  useEffect(() => {
    getFoods(token);
  }, [token]);

  const getFoods = (token: string) => {
    Axios.get("http://localhost:8000/food", {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => {
        let data = res.data;
        setFoods(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const postFood = (
    token: string,
    name: string,
    unit: string,
    amount: number
  ) => {
    Axios.post(
      "http://localhost:8000/food",
      {
        name: name,
        unit: unit,
        amount: amount,
      },
      {
        headers: { Authorization: `Bearer ${token}` },
      }
    )
      .then((res) => {
        toast.success("Food Sucessfully added");
        getFoods(token);
        setOpen(false);
      })
      .catch((err) => {
        const errorMessage = err.response.data.error;
        toast.error(errorMessage);
      });
  };

  if (token) {
    return (
      <Grid container className={styles.main}>
        <ToastContainer />
        <Header />
        <Grid container className={styles.addFoodButtonContainer}>
          <Button
            variant="contained"
            className={styles.addFoodButton}
            onClick={() => setOpen(true)}
            size="large"
          >
            {" "}
            Add food{" "}
          </Button>
          {open ? (
            <AddFoodModal
              open={open}
              postFood={postFood}
              handleClose={() => setOpen(false)}
            />
          ) : (
            ""
          )}
        </Grid>
        {foods[0] ? (
          foods.map((food: FoodEntry) => (
            <FoodCard
              key={food.id}
              id={food.id}
              food={food}
              getFoods={getFoods}
            />
          ))
        ) : (
          <Typography className={styles.addFoodButtonText} variant="h5">
            Use the Button to add food !
          </Typography>
        )}
      </Grid>
    );
  } else {
    return (
      <Container className={styles.main}>
        <Typography>Unauthorized access</Typography>
      </Container>
    );
  }
};

export default Index;
