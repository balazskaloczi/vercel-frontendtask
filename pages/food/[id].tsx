import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import {
  Typography,
  Button,
  Card,
  CardContent,
  CardActions,
  Grid,
} from "@mui/material";
import styles from "../../styles/SelectedFood.module.css";
import Axios from "axios";
import { loadState } from "../../components/localStorageStates";
import { FoodEntry } from "../../interfaces";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "../../components/header";

const SelectedFood = () => {
  let token = loadState("token");
  const router = useRouter();
  const { id } = router.query;
  const [selectedFood, setSelectedFood] = useState<FoodEntry>();

  const getSelectedFood = async (
    token: string,
    id: string | string[] | undefined
  ) => {
    await Axios.get(`http://localhost:8000/food/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => {
        let data = res.data;
        setSelectedFood(data);
      })
      .catch((err) => {
        const errorMessage = err.response.data.error;
        toast.error(errorMessage);
      });
  };

  useEffect(() => {
    getSelectedFood(token, id);
  }, [token, id]);

  return (
    <>
      <Header />
      <Grid container className={styles.main}>
        <Grid>
          {selectedFood ? (
            <Card variant="outlined" className={styles.selectedCard}>
              <CardContent>
                <Typography variant="h4">{`Food Name : ${selectedFood.name}`}</Typography>
                <Typography variant="h5">{`Food Created : ${selectedFood.createdAt}`}</Typography>
                <Typography variant="h5">{`Food ID : ${selectedFood.id}`}</Typography>
                <Typography variant="h6">Food Details :</Typography>
                <ul className={styles.foodDetailsUL}>
                  <li>
                    <Typography variant="subtitle1">{`Food Amount : ${selectedFood.details.foodDetails.unit}`}</Typography>
                  </li>
                  <li>
                    <Typography variant="subtitle1">{`Food Unit : ${selectedFood.details.foodDetails.amount}`}</Typography>
                  </li>
                </ul>
                <CardActions>
                  <Button variant="outlined" onClick={() => router.push("/")}>
                    Back
                  </Button>
                </CardActions>
              </CardContent>
            </Card>
          ) : (
            <Typography variant="h1">Please wait</Typography>
          )}
        </Grid>
        <ToastContainer />
      </Grid>
    </>
  );
};

export default SelectedFood;
