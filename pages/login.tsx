import {
  Container,
  TextField,
  Typography,
  Button,
  Grid,
  IconButton,
  InputAdornment,
  OutlinedInput,
  InputLabel,
} from "@mui/material";
import React, { useState } from "react";
import { useRouter } from "next/router";
import styles from "../styles/Login.module.css";
import Axios from "axios";
import { loginDataType } from "../interfaces";
import Header from "../components/header";
import { saveState } from "../components/localStorageStates";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

const Login = () => {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const router = useRouter();
  const [data, setData] = useState<loginDataType>({
    username: "",
    password: "",
  });

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    Axios.post("http://localhost:8000/login", {
      username: data.username,
      password: data.password,
    })
      .then((res) => {
        toast.success("Login Successful");
        saveState(res.data.token);
        setData({
          username: "",
          password: "",
        });
        setTimeout(() => {
          router.push("/");
        }, 2000);
      })
      .catch((err) => {
        const errorMessage = err.response.data.error;
        toast.error(errorMessage);
      });
  };

  const handleValueChange = (
    e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const dataInput: loginDataType = { ...data };
    const target = e.target as HTMLTextAreaElement;
    const value = target.value as string;
    const id = target.id as keyof loginDataType;
    dataInput[id] = value;
    setData(dataInput);
  };

  return (
    <Container className={styles.main}>
      <ToastContainer />
      <Header />
      <Grid container spacing={8} className={styles.mainGrid}>
        <Typography variant="h4" className={styles.loginMainText}>
          Please login for the app
        </Typography>
        <form onSubmit={(e) => handleSubmit(e)}>
          <Grid item className={styles.button}>
            <InputLabel htmlFor="username">Username</InputLabel>
            <TextField
              id="username"
              className={styles.userName}
              value={data.username}
              placeholder={!data.username ? "Please type in your username" : ""}
              variant="outlined"
              onChange={(e) => handleValueChange(e)}
            />
          </Grid>
          <Grid item className={styles.button}>
            <InputLabel htmlFor="password">Password</InputLabel>
            <OutlinedInput
              id="password"
              type={isPasswordVisible ? "text" : "password"}
              value={data.password}
              placeholder={!data.password ? "Please type your password" : ""}
              onChange={(e) => handleValueChange(e)}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    onClick={() => setIsPasswordVisible(!isPasswordVisible)}
                    edge="end"
                    aria-label="toggle password visibility"
                  >
                    {isPasswordVisible ? (
                      <VisibilityOffIcon />
                    ) : (
                      <VisibilityIcon />
                    )}
                  </IconButton>
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item className={styles.loginButton}>
            <Button type="submit" variant="contained">
              Login
            </Button>
          </Grid>
        </form>
      </Grid>
    </Container>
  );
};

export default Login;
