import React from 'react'
import styles from '../styles/ErrorNotFound.module.css'
import { Container, Typography, Grid, Button } from '@mui/material';
import { useRouter } from 'next/router'

const ErrorNotFound = () => {
  const router = useRouter();

  return (
    <>
      <Container className={styles.main}>
        <Grid className={styles.wrapper}>
          <Typography variant="h3" className={styles.errorNumber}>404</Typography>
          <iframe src="https://giphy.com/embed/pPhyAv5t9V8djyRFJH" className={styles.gif} />
          <Typography variant="h4" className={styles.obamaText}>Obama couldnt find this route</Typography>
          <Button variant="outlined" onClick={() => router.push('/')}>Back to Home ?</Button>
        </Grid>
      </Container>
    </>
  )
}

export default ErrorNotFound