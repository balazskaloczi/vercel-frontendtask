import React from "react";
import { useRouter } from "next/router";
import { Grid, Typography, Button } from "@mui/material";
import styles from "../styles/Header.module.css";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import DisabledByDefaultIcon from "@mui/icons-material/DisabledByDefault";
import { loadState, saveState } from "./localStorageStates";

const Header = () => {
  const router = useRouter();
  const token = loadState("token");

  const logOffFunction = () => {
    saveState("");
    router.push("/login");
  };

  return (
    <Grid
      className={token ? styles.header : `${styles.header} ${styles.flexEnd}`}
    >
      {token ? (
        <>
          <Button
            variant="outlined"
            className={styles.logOffButton}
            onClick={() => {
              logOffFunction();
            }}
          >
            Log Off
          </Button>
          <Grid item className={styles.loginState}>
            <Typography variant="subtitle1">Logged In</Typography>
            <Grid item>
              <CheckBoxIcon fontSize="large" />
            </Grid>
          </Grid>
        </>
      ) : (
        <Grid item className={styles.loginState}>
          <Typography variant="subtitle1">Logged Out</Typography>
          <Grid item>
            <DisabledByDefaultIcon fontSize="large" />
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

export default Header;
