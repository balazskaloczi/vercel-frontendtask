export const loadState = (state: any) => {
  try {
    const serializedState = localStorage.getItem(state);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (token: string) => {
  try {
    const serializedState = JSON.stringify(token);
    localStorage.setItem("token", serializedState);
  } catch (err) {
    return undefined;
  }
};
