import { Modal, Typography, Box, TextField, Grid, Button } from "@mui/material";
import { useState } from "react";
import styles from "../styles/FoodModal.module.css";
import { loadState } from "./localStorageStates";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

interface Props {
  open: boolean;
  handleClose: () => void | boolean;
  postFood: Function;
}

const AddFoodModal = ({ open, handleClose, postFood }: Props) => {
  const [name, setName] = useState<string>("");
  const [unit, setUnit] = useState<string>("");
  const [amount, setAmount] = useState<number>(0);
  let token = loadState("token");

  const PostAndClearInput = (
    token: String,
    name: string,
    unit: string,
    amount: number
  ) => {
    postFood(token, name, unit, amount);
    setName("");
    setUnit("");
    setAmount(0);
  };

  return (
    <>
      <Modal open={open} onClose={handleClose}>
        <Box className={styles.modalBox}>
          <form>
            <Grid container className={styles.foodModalBox}>
              <Grid>
                <TextField
                  id="name"
                  label="Food Name"
                  variant="outlined"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Grid>
              <Grid>
                <TextField
                  id="unit"
                  label="Food Unit"
                  variant="outlined"
                  value={unit}
                  onChange={(e) => setUnit(e.target.value)}
                />
              </Grid>
              <Grid>
                <TextField
                  id="amount"
                  label="Food Amount"
                  variant="outlined"
                  type="number"
                  value={amount}
                  onChange={(e) => setAmount(parseInt(e.target.value))}
                />
              </Grid>
              <Typography className={styles.ModalDescription} sx={{ mt: 2 }}>
                Please Provide the food&lsquo;s data
              </Typography>
              <Button
                variant="contained"
                onClick={() => PostAndClearInput(token, name, unit, amount)}
                size="large"
              >
                Add
              </Button>
            </Grid>
          </form>
        </Box>
      </Modal>
      <ToastContainer />
    </>
  );
};

export default AddFoodModal;
