import {
  Typography,
  Button,
  Card,
  CardContent,
  CardActions,
  Grid,
} from "@mui/material";
import { useState } from "react";
import styles from "../styles/FoodCard.module.css";
import { loadState } from "../components/localStorageStates";
import EditFoodModal from "./EditFoodModal";
import DeleteFoodModal from "./DeleteFoodModal";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import { useRouter } from "next/router";
import { FoodEntry } from "../interfaces";

interface Props {
  food: FoodEntry;
  getFoods: (token: string) => void;
  id: string;
}

const FoodCard = ({ food, getFoods, id }: Props) => {
  const router = useRouter();
  const [editModalOpen, setEditModalOpen] = useState<boolean>(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState<boolean>(false);
  let token = loadState("token");

  const moveToSpecifiedFood = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (id) {
      e.preventDefault();
      router.push(`/food/${food.id}`);
    } else {
      console.log("Something wrong");
    }
  };

  return (
    <Card className={styles.foodCardClass}>
      <Grid className={styles.moveToFoodIcon}>
        <Button
          className={styles.moveToFoodIcon}
          onClick={(e) => moveToSpecifiedFood(e)}
        >
          <ArrowUpwardIcon />
        </Button>
      </Grid>
      <CardContent>
        <Typography variant="subtitle1">Food Name: {food.name}</Typography>
        <Typography variant="subtitle2">Food Details : </Typography>
        <ul className={styles.foodDetails}>
          <li>
            <Typography variant="subtitle2">
              {food.details.foodDetails.unit}
            </Typography>
          </li>
          <li>
            <Typography variant="subtitle2">
              {food.details.foodDetails.amount}
            </Typography>
          </li>
        </ul>
        <CardActions className={styles.cardActions}>
          <Button
            size="small"
            variant="outlined"
            onClick={() => setEditModalOpen(true)}
          >
            Edit
          </Button>
          <Button
            size="small"
            variant="outlined"
            onClick={() => setDeleteModalOpen(true)}
          >
            Delete
          </Button>
          {editModalOpen ? (
            <EditFoodModal
              getFoods={getFoods}
              name={food.name}
              id={food.id}
              unit={food.details.foodDetails.unit}
              amount={food.details.foodDetails.amount}
              open={editModalOpen}
              handleClose={() => setEditModalOpen(false)}
            />
          ) : (
            ""
          )}
          {deleteModalOpen ? (
            <DeleteFoodModal
              getFoods={getFoods}
              id={food.id}
              open={deleteModalOpen}
              token={token}
              handleClose={() => setDeleteModalOpen(false)}
            />
          ) : (
            ""
          )}
        </CardActions>
      </CardContent>
    </Card>
  );
};

export default FoodCard;
