import { Modal, Typography, Box, Grid, Button } from "@mui/material";
import styles from "../styles/DeleteFoodModal.module.css";
import Axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

interface Props {
  getFoods: (token: string) => void;
  id: string;
  open: boolean;
  token: string;
  handleClose: () => void | boolean;
}

const DeleteFoodModal = ({ getFoods, id, open, token, handleClose }: Props) => {
  const deleteFood = (id: string, token: string) => {
    Axios.delete(`http://localhost:8000/food/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => {
        toast.success("Item Deleted");
        getFoods(token);
      })
      .catch((err) => {
        const errorMessage = err.response.data.error;
        toast.error(errorMessage);
      });
  };

  return (
    <>
      <Modal open={open} onClose={handleClose}>
        <Box className={styles.modalBox}>
          <Grid container className={styles.deleteFoodModalBox}>
            <Typography
              variant="h6"
              className={styles.ModalDescription}
              sx={{ mt: 2 }}
            >
              Are you sure you want to delete this item?
            </Typography>
            <Button
              className={styles.deleteFoodButton}
              variant="contained"
              onClick={() => deleteFood(id, token)}
              size="large"
            >
              Delete
            </Button>
          </Grid>
        </Box>
      </Modal>
      <ToastContainer />
    </>
  );
};

export default DeleteFoodModal;
