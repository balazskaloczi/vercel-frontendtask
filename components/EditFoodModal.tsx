import { Modal, Typography, Box, TextField, Grid, Button } from "@mui/material";
import { useState } from "react";
import styles from "../styles/FoodModal.module.css";
import Axios from "axios";
import { loadState } from "../components/localStorageStates";
import { addFoodObject } from "../interfaces";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

interface Props {
  open: boolean;
  name: string;
  id: string;
  unit: string;
  amount: number;
  handleClose: () => void | boolean;
  getFoods: (token: string) => void;
}

const EditFoodModal = ({
  open,
  name,
  unit,
  id,
  amount,
  handleClose,
  getFoods,
}: Props) => {
  const [editedName, setEditedName] = useState<string>("");
  const [editedUnit, setEditUnit] = useState<string>("");
  const [editedAmount, setEditedAmount] = useState<number>(0);
  let token = loadState("token");

  let [data, setData] = useState<addFoodObject>({
    name: name,
    unit: unit,
    amount: amount,
  });

  const modifyFood = (
    token: string,
    id: string,
    editedName: string,
    editedUnit: string,
    editedAmount: number,
    data: addFoodObject
  ) => {
    Axios.put(
      `http://localhost:8000/food/${id}`,
      {
        name: editedName ? editedName : data.name,
        unit: editedUnit ? editedUnit : data.unit,
        amount: editedAmount ? editedAmount : data.amount,
      },
      {
        headers: { Authorization: `Bearer ${token}` },
      }
    )
      .then((res) => {
        if (res) {
          toast.success("Object Modified");
        }
        getFoods(token);
        handleClose();
      })
      .catch((err) => {
        const errorMessage = err.response.data.error;
        toast.error(errorMessage);
      });
  };

  return (
    <>
      <Modal open={open} onClose={handleClose}>
        <Box className={styles.modalBox}>
          <form>
            <Grid container className={styles.foodModalBox}>
              <Grid>
                <TextField
                  id="name"
                  defaultValue={name}
                  label="Food Name"
                  variant="outlined"
                  onChange={(e) => setEditedName(e.target.value)}
                />
              </Grid>
              <Grid>
                <TextField
                  id="unit"
                  defaultValue={unit}
                  label="Food Unit"
                  variant="outlined"
                  onChange={(e) => setEditUnit(e.target.value)}
                />
              </Grid>
              <Grid>
                <TextField
                  id="amount"
                  defaultValue={amount}
                  type="number"
                  label="Food Amount"
                  variant="outlined"
                  onChange={(e) => setEditedAmount(parseInt(e.target.value))}
                />
              </Grid>
              <Typography className={styles.ModalDescription} sx={{ mt: 2 }}>
                Change the required food attribute
              </Typography>
              <Button
                variant="contained"
                onClick={() =>
                  modifyFood(
                    token,
                    id,
                    editedName,
                    editedUnit,
                    editedAmount,
                    data
                  )
                }
                size="large"
              >
                Modify
              </Button>
            </Grid>
          </form>
        </Box>
      </Modal>
      <ToastContainer />
    </>
  );
};

export default EditFoodModal;
